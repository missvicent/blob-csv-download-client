/*
Generate a csv file from a blob
data: Response from api service
msSaveBlob: Saves the File or Blob to disk.
msSaveOrOpenBlob: Launches the associated application for a File or Blob.
If you are using Angular 4,5*, remember include  {responseType: 'blob'} on your service as response!!
*/

downloadFile(data){
    let blob = new Blob([data], { type: 'text/plain' });
    if (window.navigator.msSaveOrOpenBlob)  // IE hack;
      window.navigator.msSaveBlob(blob, "user.csv");
    else
    {
      var a = window.document.createElement("a");
      a.href = window.URL.createObjectURL(blob);
      a.download = "user.csv";
      document.body.appendChild(a);
      a.click();  // IE: "Access is denied";
      document.body.removeChild(a);
    }
  }